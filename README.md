# CalculatorFX

This is a simple JavaFX application with an calculator for term processing.

[![Version](https://img.shields.io/badge/Version-1.0-success)](https://gitlab.com/s-heim/calculatorfx/-/releases) 
[![MIT License](https://img.shields.io/badge/License-MIT-orange)](http://opensource.org/licenses/MIT) 

## Mathematical Term Calculation

This applications offers a GUI to type in a mathematical term. It must be fully parenthesized and allows the following operations:
- Addition (+)
- Subtraction (-)
- Multiplication (* or x)
- Division (/ or :)

| ![Sample Term](media/calculator.jpg) | ![Support of float numbers](media/calculator2.jpg) | ![Invalid Argument](media/calculator3.jpg) |
| ------------------------------------ | -------------------------------------------------- | ------------------------------------------ |

## License

[MIT © SH](LICENSE)
