package application;

import application.calculator.Calculator;

import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class CalculatorController {

    @FXML private VBox root;
    @FXML private TextField taskEntry;
    @FXML private Label output;

    @FXML
    private void calculate() 
    {
        try 
        {
            String result = Double.valueOf(new Calculator().run(this.taskEntry.getText())).toString();
            if (result.endsWith(".0")) 
            {
                result = result.substring(0, result.length() - 2);
            } 

            output.setText("Result = " + result);
        }
        catch (Exception e) 
        {
            output.setText(e.getMessage());
        }
    }

    @FXML
    private void close() 
    {
        ((Stage) this.root.getScene().getWindow()).close();
    }
}