package application;
	
import java.io.IOException;

import javafx.application.*;
import javafx.fxml.*;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.Scene;


public class Main extends Application {
	
    private VBox root;
    private Scene scene;

    @Override
    public void start(Stage stage) throws IOException 
    {
        Main.setFrame(stage, "Main", "icon.png"); 

        Button btn = new Button("Open Calculator");
        btn.setOnAction(event -> this.openCalculator());
        this.root = new VBox(20.0, new Label("Main Window"), btn);
        this.root.setAlignment(Pos.TOP_CENTER);
        this.root.setPadding(new Insets(20, 20, 20, 20));

        this.scene = new Scene(this.root, 500, 300);
 
        stage.setScene(scene);
        stage.show();
    }

    private void openCalculator() 
    {
        try 
        {
            Stage stage = Main.setFrame(new Stage(), "Calculator", "icon.png"); 

            Scene newScene = new Scene(Main.loadFXML("calculator"), 400, 250);
            stage.setScene(newScene);
            
            stage.show();
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }
    }

    static Parent loadFXML(String fxml) throws IOException 
    {
        return (new FXMLLoader(Main.class.getResource(fxml + ".fxml"))).load();
    }

    static Stage setFrame(Stage stage, String title, String icon) {
        stage.setTitle(title);
        stage.getIcons().add(new Image(Main.class.getResourceAsStream(icon))); 
        return stage;
    }

    public static void main(String[] args) 
    {
        Main.launch();
    }
}
