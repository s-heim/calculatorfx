package application.stack;

public class StackException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected StackException() {
        super();
    }

    protected StackException(String message) {
        super(message);
    }
}
