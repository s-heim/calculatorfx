package application.stack;

public class Stack<T> {
	
    private Element<T> element = null;

    public boolean isEmpty() {
        return this.element == null;
    }

    public void push(T value) {
        Element<T> newEntry = new Element<>(value);
        newEntry.setNext(this.element);
        this.element = newEntry;
    }

    public void pop() throws StackException {
        if (this.isEmpty()) throw new StackException("Stack is empty!");

        this.element = this.element.getNext();
    }

    public T top() throws StackException {
        if (this.isEmpty()) throw new StackException("Stack is empty!");

        return this.element.getValue();
    }

    public T popTop() throws StackException {
        T result = this.top();
        this.pop();
        return result;
    }

    public String toString() {
        Element<T> temp = this.element;
        if (temp == null) return "null";

        StringBuilder result = new StringBuilder(temp.getValue().toString());
        while ((temp = temp.getNext()) != null) {
            result.append(", " + temp.getValue().toString());
        }
        return result.toString();
    }
}
