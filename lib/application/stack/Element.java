package application.stack;

public class Element<T> {
	
    private final T value;
    private Element<T> next = null;

    protected Element(T val) {
        this.value = val;
    }

    protected T getValue() {
        return this.value;
    }

    protected void setNext(Element<T> next) {
        this.next = next;
    }

    protected Element<T> getNext() {
        return this.next;
    }
    
}
