package application.calculator;

import application.stack.Stack;
import application.stack.StackException;

public class Calculator 
{
    public static int RoundedDecimalPlaces = 4;
    private static long roundVal = (long) Math.pow(10, Calculator.RoundedDecimalPlaces);

    private Stack<Character> operators;
    private Stack<Double> numbers;

    public Calculator() {
        this.operators = new Stack<>();
        this.numbers = new Stack<>();
    }

    public boolean isEmpty() {
        return this.operators.isEmpty() && this.numbers.isEmpty();
    }

    public double run(String task) throws CalculatorException, StackException, IllegalArgumentException {
        if (!this.isEmpty()) throw new CalculatorException("The stacks of this instance are not empty!");
        task = task.replaceAll("\\s", "").toLowerCase();

        char currentChar, op;
        double num1, num2;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < task.length(); i++) {
            currentChar = task.charAt(i); 

            if (Character.isDigit(currentChar) || currentChar == '.') {
                currentNumber.append(currentChar);
            }
            else {
                if (currentNumber.length() != 0) {
                    this.numbers.push(Double.valueOf(currentNumber.toString()));
                    currentNumber = new StringBuilder();
                }
    
                if (currentChar != '(') {
                    if (currentChar == ')') { 
                        num1 = this.numbers.popTop().doubleValue();
                        num2 = this.numbers.popTop().doubleValue();
                        op = this.operators.popTop().charValue();
                        switch (op) {
                            case '+': 
                                this.numbers.push(num2 + num1);
                                break;
                            case '-': 
                                this.numbers.push(num2 - num1);
                                break;
                            case '*': case 'x':
                                this.numbers.push(num2 * num1);
                                break;
                            case '/': case ':':
                                this.numbers.push(num2 / num1);
                                break;
                            default: throw new CalculatorException("Something went wrong while calculating!");
                        }
                    }
                    else if ("+-*x/:".indexOf(currentChar) != -1) {
                        this.operators.push(currentChar);
                    }
                    else throw new IllegalArgumentException(String.format("Unexpected Character '%c' found!", currentChar));
                }
            } 
        }

        double result = (double) Math.round(this.numbers.popTop() * Calculator.roundVal) / Calculator.roundVal;

        if (!this.isEmpty()) 
            throw new CalculatorException(String.format("Expected two empty stacks, but stacks are not empty!\nCurrent result: %d\n\nNumbers: %s\nOperators: %s\n", 
                result, this.numbers.toString(), this.operators.toString()));

        return result;
    }

}
    

