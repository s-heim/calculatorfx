package application.calculator;

public class CalculatorException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	protected CalculatorException() {
        super();
    }

    protected CalculatorException(String message) {
        super(message);
    }
    
}
